﻿using UnityEngine;
using System;
using System.Collections;

namespace owllab {
    public class InputController : MonoBehaviour {

        public bool m_isTesting = false;
        private Power m_pCurrent = null;
        private BoardController m_pBoard = null;
        private static InputController s_instance;
        private Action handler;

        private void Awake() {
            s_instance = this;
            handler = OnMenu;

            if (m_isTesting) {
                handler = OnGame;
                GameObject go = GameObject.Find("board");
                m_pBoard = go.GetComponent<BoardController>();
            }
        }
        
        private void OnLevelWasLoaded(int level) {
            if (level == Consts.C_SCENE_GAME) {
                handler = OnGame;
                this.enabled = true;
                GameObject go = GameObject.Find("board");
                m_pBoard = go.GetComponent<BoardController>();
            } else {
                this.enabled = true;
                handler = OnMenu;
            }
            
        }

        private void Update() {
            handler();   
        }

        private void OnMenu() {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Menu)) {

                switch (Player.panel) {
                    case PanelType.Main:
                        Application.Quit();
                        break;
                    case PanelType.Level:
                        Player.panel = PanelType.Main;
                        UIController.OpenPanel();
                        break;
                    default: break;
                }
            }
        }

        private void OnGame() {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Q)) Application.LoadLevel(Consts.C_SCENE_MENU);
            if (Input.GetMouseButtonDown(0)) {
                RaycastHit2D hit1;
                Ray ray1 = Camera.main.ScreenPointToRay(Input.mousePosition);
                hit1 = Physics2D.GetRayIntersection(ray1, 10f);
                if (hit1.collider != null) {
                    Power power = hit1.collider.GetComponent<Power>();
                    if (power != null) {
                        Drop(power);
                    }
                }
                Debug.DrawRay(ray1.origin, ray1.direction * 10f, Color.red, 10);
            }
            if (Input.GetMouseButton(0)) {
                if (m_pCurrent != null) {
                    Drag(Input.mousePosition);
                }
            }

            if (Input.GetMouseButtonUp(0)) {
                if (m_pCurrent != null) {
                    Down(Input.mousePosition);
                }
            }
#endif

#if UNITY_ANDROID
            // переход в главное меню
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Menu))
                Application.LoadLevel(Consts.C_SCENE_MENU);

            for (int i = 0; i < Input.touchCount; ++i) {
                switch (Input.GetTouch(i).phase) {

                    case TouchPhase.Began: {
                            if (Input.GetTouch(i).fingerId > 1) return;
                            if (Input.GetTouch(i).fingerId == 0) {
                                RaycastHit2D hit;
                                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                                hit = Physics2D.GetRayIntersection(ray, 10f);
                                if (hit.collider != null) {
                                    Debug.Log(hit.collider.name);
                                    Power power = hit.collider.GetComponent<Power>();
                                    if (power != null) {
                                        Drop(power);
                                    }
                                }
                                Debug.DrawRay(ray.origin, ray.direction * 10f, Color.red, 10);
                            }
                        }
                        break;
                    case TouchPhase.Moved: {
                            if (Input.GetTouch(i).fingerId == 0 && m_pCurrent != null) {
                                Drag(Input.GetTouch(i).position);
                            }
                        }
                        break;
                    case TouchPhase.Ended: {
                            if (Input.GetTouch(i).fingerId == 0 && m_pCurrent != null) {
                                Down(Input.GetTouch(i).position);
                            }
                        }
                        break;
                    case TouchPhase.Canceled: {
                        }
                        break;
                    default: break;
                }
            }
#endif
        }

        private void Drop(Power power) {
            m_pCurrent = power;
            m_pBoard.Release(power);
            m_pCurrent.Drop();
        }

        private void Drag(Vector3 input) {
            Vector3 position = Camera.main.ScreenToWorldPoint(input);
            position.z = 0;
            m_pCurrent.Drag(position);
        }

        private void Down(Vector3 input) {
            Vector3 position = Camera.main.ScreenToWorldPoint(input);          
            HexData data = GlowUtility.PixelToHex(position);
            m_pBoard.Occupied(m_pCurrent, data);

            m_pCurrent.Down();
            m_pCurrent = null;
        }

        public static void Disable() {
            s_instance.enabled = false;
        }

    }
}