﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace owllab {
    public sealed class Localization {

        private const string ASSET_EN_LANG = "lang/en";
        private const string ASSET_RU_LANG = "lang/ru";

        private static Dictionary<Language, string> m_pPaths = new Dictionary<Language, string>() {
            {Language.En, ASSET_EN_LANG},
            {Language.Ru, ASSET_RU_LANG}
        };
        private static Dictionary<string, string> m_pWords;

        public static void LoadLang(Language lang) {
            m_pWords = new Dictionary<string, string>();
            TextAsset langAsset = (TextAsset)ResourceManager.GetPrefab(m_pPaths[lang], typeof(TextAsset));
            Dictionary<string, object> dict = Json.Deserialize(langAsset.text) as Dictionary<string, object>;

            foreach (string key in dict.Keys) {
                //Debug.Log(key + ": " + dict[key]);
                m_pWords[key] = (string)dict[key];
            }
        }

        public static string Translate(string word) {
            return (m_pWords.ContainsKey(word)) ? m_pWords[word] : "";

        }
    }
}
