﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public static class Player {

        private const string PROGRESS = "progress";
        private const string MUSIC = "music";
        private const string LANG = "lang";

        private static Language m_pLang = Language.En;
        private static bool m_pMusic = true;
        private static LevelData[] m_pLevels;
        private static int m_pCurrentLevel;
        private static int m_pProgress = 0;
        private static PanelType m_pActivePanel = PanelType.Logo;

        public static void Load() {
            //load level progress
            if (PlayerPrefs.HasKey(PROGRESS)) {
                m_pProgress = PlayerPrefs.GetInt(PROGRESS);
            }
            //load music status
            if (PlayerPrefs.HasKey(MUSIC)) {
                m_pMusic = (PlayerPrefs.GetInt(MUSIC) == 1) ? true : false;
            }

            if (PlayerPrefs.HasKey(LANG)) {
                int lang = PlayerPrefs.GetInt(LANG);
                m_pLang = (Language)lang;
            }

            //Debug.Log("progress ONLOAD: " + m_pProgress + " music: " + m_pMusic + " lang: " + m_pLang);
            //Reset();
        }

        private static void Save() {
            //save level progress
            PlayerPrefs.SetInt(PROGRESS, m_pProgress);
        }

        private static void SaveMusic() {
            PlayerPrefs.SetInt(MUSIC, m_pMusic == true?1:0);
        }

        private static void SaveLang() {
            PlayerPrefs.SetInt(LANG, (int)m_pLang);
        }

        //for testing
        public static void Reset() {
            PlayerPrefs.SetInt(PROGRESS, 0);
            PlayerPrefs.SetInt(MUSIC, 1);
            PlayerPrefs.SetInt(LANG, (int)Language.En);
        }

        public static void NextLang() {
            if (m_pLang == Language.En) m_pLang = Language.Ru;
            else m_pLang = Language.En;
            SaveLang();
        }

        public static Language lang { get { return m_pLang; } }
        public static PanelType panel { get { return m_pActivePanel; } set { m_pActivePanel = value; } }
        public static int currentLevel { get { return m_pCurrentLevel; } set { m_pCurrentLevel = value; } }
        public static bool enableMusic { get { return m_pMusic; } set { m_pMusic = value; SaveMusic(); } }
        public static LevelData[] levels { get { return m_pLevels; } set { m_pLevels = value; } }
        public static LevelData level { get { return m_pLevels[m_pCurrentLevel]; } }
        public static int progress {
            get { return m_pProgress; }
            set {
                if (value <= m_pProgress) return;
                m_pProgress = value;
                Save();
            }
        }
    }
}
