﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace owllab {

    public class LevelButton : MonoBehaviour {

        private readonly Color ACTIVE_COLOR = new Color(65f / 255f, 68f / 255f, 68f / 255f, 1f);
        private readonly Color CLOSE_COLOR = new Color(110f / 255f, 203f / 255f, 217f / 255f, 1f);

        private Image m_pImage;
        private Text m_pText;
        public Sprite m_sp;
        public int m_num;

        // Use this for initialization
        private void Start() {
            m_pText = transform.GetComponentInChildren<Text>();
            m_pImage = transform.GetComponent<Image>();

            //if (m_pText != null) Debug.Log("not null");

            m_pText.text = m_num.ToString();

            if (Player.progress >= m_num-1) {
                m_pText.color = ACTIVE_COLOR;
                    
            } else {
                m_pText.color = CLOSE_COLOR;
                m_pImage.sprite = m_sp;

            }
        }
    }

}