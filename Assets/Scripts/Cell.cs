﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public class Cell : MonoBehaviour {

        public Sprite m_active;
        public Sprite m_deactive;

        private SpriteRenderer m_pSpriteRender;
        //private Sprite m_pSprite; 
        private CellData m_pData;
        private int m_pPowerCount = 0;
        private bool m_pIsOccupied = false;

        private void Start() {
            m_pSpriteRender = GetComponent<SpriteRenderer>();
        }

        public void Occupied() {
            m_pPowerCount++;
            Activate();
            
            m_pIsOccupied = true;
            Debug.Log("count: " + m_pPowerCount);
        }

        public void Release() {
            m_pIsOccupied = false;
            m_pPowerCount--;
            Deactive();
            
            Debug.Log("count: " + m_pPowerCount);
        }

        public void Light(float delay) {
            m_pPowerCount++;
            StartCoroutine(LightNow(delay));    
        }

        public IEnumerator LightNow(float delay) {
            yield return new WaitForSeconds(delay);
            Activate();      
        }

        public void Delight(float delay) {
            m_pPowerCount--;
            StartCoroutine(DelightNow(delay));
        }

        public IEnumerator DelightNow(float delay) {
            yield return new WaitForSeconds(delay);
            Deactive();
        }

        private void Activate() {
            if (m_pPowerCount >= 1)
                m_pSpriteRender.sprite = m_active;
        }

        private void Deactive() {
           
            if (m_pPowerCount == 0) 
                m_pSpriteRender.sprite = m_deactive;
        }

        public CellData data { get { return m_pData; } set { m_pData = value; } }
        public CellType type { get { return m_pData.type; } }
        public int q { get { return m_pData.q; } }
        public int r { get { return m_pData.r; } }
        public bool isOccupied { get { return m_pIsOccupied; } }
        public bool isLight { get { return m_pPowerCount > 0; } }
    }
}
