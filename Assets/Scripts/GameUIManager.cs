﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace owllab {
    public class GameUIManager : MonoBehaviour {

        public Text m_levelTxt;
        public GameObject m_congPanel;
        public GameObject m_tutPanel;
        public GameObject m_votePanel;

        public Text m_cong;
        public Text m_vote;
        public Text m_tut1;
        public Text m_tut2;
        public Text m_tut3;

        private static GameUIManager m_pSelf;

        private void Awake() {
            //m_congPanel.SetActive(false);
            m_tutPanel.SetActive(false);
            m_votePanel.SetActive(false);
            m_pSelf = this;
        }

        private void Start() {
            m_levelTxt.text = (Player.currentLevel + 1).ToString() + " " + Localization.Translate(Consts.W_CURR_LEVEL);

            if (Player.currentLevel == Consts.C_LEVEL_TO_SHOW_TUT) ShowTut();
        }

        public static void Cong() {
            if (Player.currentLevel == Consts.C_LEVEL_TO_SHOW_VOTE) {
                m_pSelf.m_votePanel.SetActive(true);
                m_pSelf.m_vote.text = Localization.Translate(Consts.W_VOTE);
            } else {
                m_pSelf.ShowCong();
            }
        }

        public void onVote() {
            Application.OpenURL(Consts.URL_VOTE);
            CloseVotePanel();
        }

        public void CloseVotePanel() {
            m_votePanel.SetActive(false);
            ShowCong();
        }

        public void ShowCong() {
            m_congPanel.SetActive(true);


            Animation anim = m_congPanel.GetComponent<Animation>();
            anim.Play();

            m_cong.text = Localization.Translate(Consts.W_CONG);

        }

        public void ShowTut() {
            m_tutPanel.SetActive(true);
            m_tut1.text = Localization.Translate(Consts.W_TUT_1);
            m_tut2.text = Localization.Translate(Consts.W_TUT_2);
            m_tut3.text = Localization.Translate(Consts.W_TUT_3);
        }

        public void OpenMenu() {
            Application.LoadLevel(Consts.C_SCENE_MENU);
        }

        public void OnClick() {
            SoundManager.Play(SoundType.Click);
        }

        public void CloseTutPanel() {
            m_tutPanel.SetActive(false);
        }

        public void NextLevel() {
            Player.currentLevel++;
            if (Player.currentLevel < Player.levels.Length)
                Application.LoadLevel(Consts.C_SCENE_GAME);
        }
    }
}
