﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace owllab {

    public enum PanelType {
        Main = 0,
        Level = 1,
        Logo = 2
    }

    public enum SoundType {
        Unknow = -1,
        Success = 0,
        Occupied = 1,
        Click = 2,
        ToSlot = 3
    }

    public enum Language {
        En = 0,
        Ru = 1,
        It = 2,
        Sp = 3,
        Ger = 4
    }

    public enum CellType {
        Unknow = -1,
        Basic = 0,
        Volt = 1
    }

    public enum PowerType {
        Unknow = -1,
        Point = 0,
        Up = 1,
        Down = 2,
        UL = 3,
        DL =4,
        UR = 5,
        DR = 6,
		DL_UR = 7,
		UL_DR = 8,
		Up_Down = 9,
		UL_UR = 10,
		U_DR = 11,
		UR_D = 12,
		DL_DR = 13,
		UL_D = 14,
		DL_U = 15,
		UL_D_UR = 16,
		DL_U_DR = 17,
		ALL = 18
    }

    public struct PowerData {
        public PowerType type;

        public PowerData(PowerType type) {
            this.type = type;
        }
    }

    public struct HexData {
        public int q;
        public int r;
        public HexData(int q, int r) {
            this.q = q;
            this.r = r;
        }
    }

    public struct CellData {
        public CellType type;
        public HexData data;

        public CellData(CellType type, int q, int r) {
            this.type = type;
            this.data = new HexData(q,r);
        }

        public int q { get { return data.q; } }
        public int r { get { return data.r; } }
    }

    public struct LevelData {
        public CellData[] cells;
        public PowerData[] powers;

        public LevelData(CellData[] cells, PowerData[] powers) {
            this.cells = cells;
            this.powers = powers;
        }
    }

    public static class GlowUtility {

        public static HexData PixelToHex(Vector3 point) {
            int q = Mathf.RoundToInt(point.x / (Consts.HEX_SIZE * 1.5f));
            int r = Mathf.RoundToInt(point.y / ((-1) * Consts.HEX_SIZE * Mathf.Sqrt(3)) - q * 0.5f);
            return new HexData(q,r);
        }

        public static Vector2 HexToPixel(HexData data) {
            float x = Consts.HEX_SIZE * 1.5f * data.q;
            float y = (-1) * Consts.HEX_SIZE * Mathf.Sqrt(3) * (data.r + data.q * 0.5f);
            return new Vector2(x,y);
        }

        public static LevelData[] LoadLevel(Dictionary<string, object> dict) {
            LevelData[] levels = new LevelData[dict.Count];
            foreach (string key in dict.Keys) {
                Dictionary<string, object> level = dict[key] as Dictionary<string, object>;
                List<object> cells = level["cells"] as List<object>;
                CellData[] cls = new CellData[cells.Count];
                for (int i = 0; i < cells.Count; ++i) {
                    List<object> param = cells[i] as List<object>;

                    CellType type = (CellType) System.Convert.ToInt32(param[0]);
                    int q = System.Convert.ToInt32(param[1]);
                    int r = System.Convert.ToInt32(param[2]);
                    cls[i] = new CellData(type, q, r);
                }

                List<object> powers = level["powers"] as List<object>;
                PowerData[] pws = new PowerData[powers.Count];
                for (int i = 0; i < powers.Count; ++i) {
                    PowerData powerData = new PowerData((PowerType)System.Convert.ToInt32(powers[i]));
                    pws[i] = powerData;
                }

                levels[System.Convert.ToInt32(key)] = new LevelData(cls, pws);
            }
            return levels;
        }
    }

}
