﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace owllab {

    public class BoardController : MonoBehaviour {

        public bool m_isTest;
        public int m_currLevelTest;
        public Transform m_power;
        public Dictionary<HexData, Cell> m_cells = new Dictionary<HexData, Cell>();
        private delegate void Light(Cell cell, float delay);

        private void Start() {
            
            
            //TODO: delete this for testing
#if UNITY_EDITOR
            if (m_isTest) {
                Player.currentLevel = m_currLevelTest;
            }
#endif
            //Debug.Log("player curr level: " + Player.currentLevel);
            BuildLevel();
        }

        private void BuildLevel() {

            LevelData level = Player.level;
            
            //add cells
            Transform hex = (Transform)ResourceManager.GetPrefab(Consts.PREFAB_HEXAGON,typeof(Transform));
            foreach (CellData cellData in level.cells) {

                Vector2 pos = GlowUtility.HexToPixel(cellData.data);

                Transform go = (Transform)Instantiate(hex, new Vector3(pos.x, pos.y, 0f), Quaternion.identity);
                Cell cell = go.GetComponent<Cell>();
                go.parent = transform;
                cell.data = cellData;

                m_cells[cellData.data] = cell;
            }

            //add powers
            GameObject powerPrefab = (GameObject)ResourceManager.GetPrefab(Consts.PREFAB_POWER, typeof(GameObject));
            int count = level.powers.Length;
            float xStart;
            float r = 1.5f + 1;
            if (count % 2 == 0) {
                xStart = -(r * 0.5f) * (count - 1);
            } else {
                xStart = -((int)(count * 0.5f)) * r;
            }

            for (int i = 0; i < count; ++i) {
                Vector3 pos = new Vector3(0, 0, 0);
                pos.x = xStart + i * r;
                pos.y = -18f;
                GameObject go = (GameObject)Instantiate(powerPrefab, pos, Quaternion.identity);
                Power power = go.GetComponent<Power>();
                power.type = level.powers[i].type;
                power.sprite = ResourceManager.GetPowerSprite(power.type);
                go.transform.parent = m_power;
            }
            
        }

        public void Occupied(Power power, HexData key) {
            if (!m_cells.ContainsKey(key) || m_cells[key].isOccupied) {
                power.ToSlot();
                return;
            }
            SoundManager.Play(SoundType.Occupied);
            Debug.Log("Occupied");
            m_cells[key].Occupied();
            power.hexPosition = key;
            power.ToCell();
            Debug.Log(power.type);
            float delay = Consts.C_DELAY;
            Light cb = (Cell cell, float d) => { cell.Light(d); };
            LightAction(power, ref delay,cb);
            delay += Consts.C_DELAY;
            StartCoroutine(Check(delay));
        }

        private void up(Light action, HexData key, ref float d) {
            key.r--;
            while (m_cells.ContainsKey(key)) {

                action(m_cells[key], d);
                key.r--;
                d += Consts.C_DELAY;
            }
        }

        private void down(Light action, HexData key, ref float d) {
            key.r++;
            while (m_cells.ContainsKey(key)) {
                action(m_cells[key], d);
                key.r++;
                d += Consts.C_DELAY;
            }
        }

        private void upLeft(Light action, HexData key, ref float d) {
            key.q--;
            while (m_cells.ContainsKey(key)) {
                action(m_cells[key], d);
                key.q--;
                d += Consts.C_DELAY;
            }
        }

        private void downLeft(Light action, HexData key, ref float d) {
            key.r++;
            key.q--;
            while (m_cells.ContainsKey(key)) {
                action(m_cells[key], d);
                key.r++;
                key.q--;
                d += Consts.C_DELAY;
            }
        }

        private void upRight(Light action, HexData key, ref float d) {
            key.r--;
            key.q++;
            while (m_cells.ContainsKey(key)) {
                action(m_cells[key], d);
                key.r--;
                key.q++;
                d += Consts.C_DELAY;
            }
        }

        private void downRight(Light action, HexData key, ref float d) {
            key.q++;
            while (m_cells.ContainsKey(key)) {
                action(m_cells[key], d);
                key.q++;
                d += Consts.C_DELAY;
            }
        }

        private void LightAction(Power power, ref float delay, Light action) {

            HexData key = power.hexPosition;

            switch (power.type) {
                case PowerType.Point:
                    break;

                case PowerType.Up:
                    up(action, key, ref delay);
                    break;

                case PowerType.Down:
                    down(action, key, ref delay);
                    break;

                case PowerType.UL:
                    upLeft(action, key, ref delay);
                    break;

                case PowerType.DL:
                    downLeft(action, key, ref delay);
                    break;

                case PowerType.UR:
                    upRight(action, key, ref delay);
                    break;

                case PowerType.DR:
                    downRight(action, key, ref delay);
                    break;

                case PowerType.DL_UR:
                    float d1 = delay;
                    float d2 = delay;
                    downLeft(action, key, ref d1);
                    upRight(action, key, ref d2);
                    delay = Math.Max(d1, d2);
                    break;

                case PowerType.UL_DR:
                    float ud1 = delay;
                    float ud2 = delay;
                    upLeft(action, key, ref ud1);
                    downRight(action, key, ref ud2);
                    delay = Math.Max(ud1, ud2);
                    break;

                case PowerType.Up_Down:
                    float udd1 = delay;
                    float udd2 = delay;
                    up(action, key, ref udd1);
                    down(action, key, ref udd2);
                    delay = Math.Max(udd1, udd2);
                    break;

                case PowerType.UL_UR:
                    float uld1 = delay;
                    float uld2 = delay;
                    upLeft(action, key, ref uld1);
                    upRight(action, key, ref uld2);
                    delay = Math.Max(uld1, uld2);
                    break;

                case PowerType.U_DR:
                    float dr1 = delay;
                    float dr2 = delay;
                    up(action, key, ref dr1);
                    downRight(action, key, ref dr2);
                    delay = Math.Max(dr1, dr2);
                    break;

                case PowerType.UR_D:
                    float d3 = delay;
                    float d4 = delay;
                    upRight(action, key, ref d3);
                    down(action, key, ref d4);
                    delay = Math.Max(d3, d4);
                    break;

                case PowerType.DL_DR:
                    float d5 = delay;
                    float d6 = delay;
                    downLeft(action, key, ref d5);
                    downRight(action, key, ref d6);
                    delay = Math.Max(d5, d6);
                    break;

                case PowerType.UL_D:
                    float d7 = delay;
                    float d8 = delay;
                    upLeft(action, key, ref d7);
                    down(action, key, ref d8);
                    delay = Math.Max(d7, d8);
                    break;

                case PowerType.DL_U:
                    float d9 = delay;
                    float d10 = delay;
                    downLeft(action, key, ref d9);
                    up(action, key, ref d10);
                    delay = Math.Max(d9, d10);
                    break;

                case PowerType.UL_D_UR:
                    float d11 = delay;
                    float d12 = delay;
                    float d13 = delay;
                    upLeft(action, key, ref d11);
                    down(action, key, ref d12);
                    upRight(action, key, ref d13);
                    delay = Math.Max(d11, d12);
                    delay = Math.Max(delay, d13);
                    break;

                case PowerType.DL_U_DR:
                    float d14 = delay;
                    float d15 = delay;
                    float d16 = delay;
                    downLeft(action, key, ref d14);
                    up(action, key, ref d15);
                    downRight(action, key, ref d16);
                    delay = Math.Max(d14, d15);
                    delay = Math.Max(delay, d16);
                    break;

                case PowerType.ALL:
                    float d17 = delay;
                    float d18 = delay;
                    float d19 = delay;
                    float d20 = delay;
                    float d21 = delay;
                    float d22 = delay;
                    up(action, key, ref d17);
                    upRight(action, key, ref d18);
                    downRight(action, key, ref d19);
                    down(action, key, ref d20);
                    downLeft(action, key, ref d21);
                    upLeft(action, key, ref d22);
                    d17 = Math.Max(d17, d18);
                    d19 = Math.Max(d19, d20);
                    d21 = Math.Max(d21, d22);
                    d17 = Math.Max(d17, d19);
                    delay = Math.Max(d17, d21);
                    break;

                default: break;

            }
        }

        private IEnumerator Check(float delay) {
            yield return new WaitForSeconds(delay);

            int all = m_cells.Count;
            int count = 0;
            foreach (HexData key in m_cells.Keys) {
                if (m_cells[key].isLight) count++;
            }
            Debug.Log("all:" + all + " count: " + count);
            if (all == count) {
                //win
                Player.progress = Player.currentLevel + 1;
                SoundManager.Play(SoundType.Success);
                InputController.Disable();
                GameUIManager.Cong();
            }
        }

        public void Release(Power power) {
            if (!m_cells.ContainsKey(power.hexPosition)) return;
            m_cells[power.hexPosition].Release();
            float delay = Consts.C_DELAY;
            Light cb = (Cell cell, float d) => { cell.Delight(d); };
            LightAction(power, ref delay, cb);

            power.hexPosition = new HexData(100,100);
        }
    }
}
