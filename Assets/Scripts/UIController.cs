﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace owllab {
    public class UIController : MonoBehaviour {

        public Text m_levelTxt;

		public Image m_logoText;
        public GameObject m_logo;
        public GameObject m_mainPanel;
        public GameObject m_levelPanel;

        public Image m_musicBtn;
        public Sprite m_musicOn;
        public Sprite m_musicOff;

        public Image m_langBtn;
        public Sprite m_en;
        public Sprite m_ru;

        private static UIController s_instance;

        private void Awake() {
            s_instance = this;
            GameObject m = GameObject.Find("managers");
            if (m == null) {
                GameObject managers = Resources.Load<GameObject>("managers");
                m = (GameObject)Instantiate(managers, Vector3.zero, Quaternion.identity);
                m.name = "managers";
            }
        }

        private void Start() {
            OpenPanel();
            SetLanguage();
            SetMusic();
        }

        private void SetLanguage() {
            m_levelTxt.text = Localization.Translate(Consts.W_LEVELS);
            m_langBtn.sprite = (Player.lang == Language.En) ? m_en : m_ru;
        }

        private void SetMusic() {
            SoundManager.music = Player.enableMusic;
            m_musicBtn.sprite = (Player.enableMusic) ? m_musicOn : m_musicOff;
        }

        private IEnumerator ShowLogo() {
            m_logoText.CrossFadeAlpha(0f, 2f, true);
            yield return new WaitForSeconds(2f);
            Player.panel = PanelType.Main;
            OpenPanel();
            yield return null;
        }

        private void OpenLogo() {
            m_logo.SetActive(true);
            m_levelPanel.SetActive(false);
            StartCoroutine(ShowLogo());
        }


        public void ChangeLang() {
            Player.NextLang();
            ResourceManager.UpdateLang();
            SetLanguage();
        }

        

        public void OpenMain() {
            m_logo.SetActive(false);
            m_mainPanel.SetActive(true);
            m_levelPanel.SetActive(false);
            Player.panel = PanelType.Main;
        }

        public void OpenLevels() {
            m_logo.SetActive(false);
            m_mainPanel.SetActive(false);
            m_levelPanel.SetActive(true);
            Player.panel = PanelType.Level;
        }

        public void LoadLevel(int levelId) {
            //check player progress
            Debug.Log("progress:" + Player.progress + " level: " + levelId);
            if (Player.progress < levelId) {
                Debug.Log("progress < levelID");
                return;
            }

            Player.currentLevel = levelId;
            Application.LoadLevel(Consts.C_SCENE_GAME);
        }

        public void OnClick() {
            SoundManager.Play(SoundType.Click);
        }

        public void MoreGames() {
            Application.OpenURL(Consts.URL_OWL_LAB_GAMES);
        }

        public void OnMusic() {
            Player.enableMusic = !Player.enableMusic;
            SetMusic();
        }

        public static void OpenPanel() {
            switch (Player.panel) {
                case PanelType.Logo:
                    s_instance.OpenLogo();
                    break;
                case PanelType.Main:
                    s_instance.OpenMain();
                    break;
                case PanelType.Level:
                    s_instance.OpenLevels();
                    break;
                default: break;
            }
        }
    }
}