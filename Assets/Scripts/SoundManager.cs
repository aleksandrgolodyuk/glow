﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public sealed class SoundManager : MonoBehaviour {

        private const float VOLUME_ON = 1f;
        private const float VOLUME_OFF = 0f;

        public AudioClip m_success;
        public AudioClip m_occupied;
        public AudioClip m_click;
        public AudioClip m_toSlot;
        
        private AudioSource m_pAudio;
        private static SoundManager s_instance;

        void Awake() {
            s_instance = this;
            m_pAudio = GetComponent<AudioSource>();
            DontDestroyOnLoad(gameObject);
        }

        public static void Play(SoundType sound) {
            if (!Player.enableMusic) return;

            AudioClip clip = null;

            switch (sound) {
                case SoundType.Success:
                    clip = s_instance.m_success;
                    break;
                case SoundType.Occupied:
                    clip = s_instance.m_occupied;
                    break;
                case SoundType.Click:
                    clip = s_instance.m_click;
                    break;
                case SoundType.ToSlot:
                    clip = s_instance.m_toSlot;
                    break;
                default: break;
            }

            if (clip != null)
                s_instance.m_pAudio.PlayOneShot(clip);
        }

        public static bool music {
            set {
                if (value) {
                    AudioListener.volume = VOLUME_ON;
                } else {
                    AudioListener.volume = VOLUME_OFF;
                }
            }
        }
        


    }
}