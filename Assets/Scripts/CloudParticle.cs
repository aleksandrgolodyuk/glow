﻿using UnityEngine;
using System.Collections;

public class CloudParticle : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "cloud";
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = 2;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
