﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public class CameraController : MonoBehaviour {

        private Camera m_pCamera;

        private static Color START_COLOR = new Color(216f/255f, 226f/255f, 1f);
        private static Color END_COLOR = new Color(188f/255f,205f/255f,1f);

        private void Awake() {
            m_pCamera = this.GetComponent<Camera>();
            m_pCamera.backgroundColor = START_COLOR;
            Debug.Log("aspect: " + Camera.main.aspect);
            
            // Формула для расчета: size = HEIGHT / 2 * PixelToUnit
            // HEIGHT- высота экрана
            // 2 - т.к. size - это половина экрана в юнитах
            // PixelToUnit - значение выставленное у спрайтов
            Camera.main.orthographicSize = (Consts.C_SCREEN_WIDTH / Camera.main.aspect) / (2 * 23f);
        }

        private bool down = false;
        private void Update() {
            Color c = START_COLOR;
            if (m_pCamera.backgroundColor != END_COLOR && !down) {
                c = END_COLOR;
                
            } else if (m_pCamera.backgroundColor == START_COLOR && down) {
                c = END_COLOR;
                down = false;

            }else if (m_pCamera.backgroundColor == END_COLOR) {
                down = true;
            } 

            m_pCamera.backgroundColor = Color.Lerp(m_pCamera.backgroundColor, c, Time.deltaTime);
        }
    }
}