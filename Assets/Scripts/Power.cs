﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public class Power : MonoBehaviour {

        private PowerData m_pData;
        private Vector3 m_pResultPosition;
        private Transform m_pTr;
        private Cell m_pCell;
        private HexData m_pHexPosition;
        private Vector3 m_pSlotPosition;
        private SpriteRenderer m_pSpriteRender;

        private void Awake() {
            m_pTr = transform;
            m_pResultPosition = position;
            m_pHexPosition = new HexData(100, 100);
            m_pSlotPosition = position;
            m_pSpriteRender = GetComponent<SpriteRenderer>();
            
        }

        public void Init() {
            m_pTr = transform;
            m_pResultPosition = position;
            m_pHexPosition = new HexData(100, 100);
            m_pSlotPosition = position;
            m_pSpriteRender = GetComponent<SpriteRenderer>();
         
        }

        private void Update() {
            if (position != m_pResultPosition) {
                position = Vector3.Lerp(position, m_pResultPosition, Time.deltaTime * Consts.POWER_SPEED);
            }
        }

        public void Drop() {

        }

        public void Drag(Vector3 newPos) {
            m_pResultPosition = newPos;
        }

        public void Down() {

        }

        public void ToSlot() {
            SoundManager.Play(SoundType.ToSlot);
            m_pResultPosition = m_pSlotPosition;
        }

        public void ToCell() {
            m_pResultPosition = GlowUtility.HexToPixel(m_pHexPosition);
        }

        public Vector3 position { get { return m_pTr.position; } set { m_pTr.position = value; } }
        public PowerType type { get { return m_pData.type; } set { m_pData.type = value; } }
        public Cell cell { get { return m_pCell; } }
        public HexData hexPosition { get { return m_pHexPosition; } set { m_pHexPosition = value; } }
        public Sprite sprite { get { return m_pSpriteRender.sprite; } set { m_pSpriteRender.sprite = value; } }
    }
}