using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace owllab {
    public class ResourceManager : MonoBehaviour {

        private Dictionary<string, Object> m_pPrefabs = new Dictionary<string, Object>();
        private Dictionary<PowerType, Sprite> m_pPowerSprites = new Dictionary<PowerType, Sprite>();
        private static Dictionary<PowerType, string> m_pPowersName = new Dictionary<PowerType, string>() {
            {PowerType.Point, "lights_point"},
            {PowerType.Up, "lights_002"},
            {PowerType.Down, "lights_005"},
            {PowerType.UL, "lights_001"},
            {PowerType.DL, "lights_006"},
            {PowerType.UR, "lights_003"},
            {PowerType.DR, "lights_004"},
            {PowerType.DL_UR, "lights_007"},
            {PowerType.UL_DR, "lights_008"},
            {PowerType.Up_Down, "lights_009"},
            {PowerType.UL_UR, "lights_010"},
            {PowerType.U_DR, "lights_011"},
            {PowerType.UR_D, "lights_012"},
            {PowerType.DL_DR, "lights_013"},
            {PowerType.UL_D, "lights_014"},
            {PowerType.DL_U, "lights_015"},
            {PowerType.UL_D_UR, "lights_016"},
            {PowerType.DL_U_DR, "lights_017"},
            {PowerType.ALL, "lights_018"}
        };

        private static ResourceManager m_pSelf;

        private void Awake() {
            m_pSelf = this;

            //load levels
            TextAsset levelsAsset = (TextAsset)ResourceManager.GetPrefab(Consts.TEXTASSET_LEVELS, typeof(TextAsset));
            string levelStr = levelsAsset.ToString();
            LevelData[] levels = GlowUtility.LoadLevel(Json.Deserialize(levelStr) as Dictionary<string, object>);
            Player.levels = levels;

            //load player progress
            Player.Load();

            //load lang 
            Localization.LoadLang(Player.lang);
        }

        public static void UpdateLang() {
            Localization.LoadLang(Player.lang);
        }

        public static Sprite GetPowerSprite(PowerType type) {
            if (!m_pSelf.m_pPowerSprites.ContainsKey(type)) {
                m_pSelf.m_pPowerSprites[type] = Resources.Load<Sprite>(m_pPowersName[type]);
            }
            return m_pSelf.m_pPowerSprites[type];
        }

        public static Object GetPrefab(string name, System.Type t) {
            if (!m_pSelf.m_pPrefabs.ContainsKey(name)) {
                m_pSelf.m_pPrefabs[name] = Resources.Load(name, t);
            }
            return m_pSelf.m_pPrefabs[name];
        }

    }
}