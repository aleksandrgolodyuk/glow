﻿using UnityEngine;
using System.Collections;

namespace owllab {
    public static class Consts {
        public const uint POWER_SPEED = 25;
        public const float HEX_SIZE = 2f;

        public const int C_SCENE_MENU = 0;
        public const int C_SCENE_GAME = 1;

        public const int C_SCREEN_WIDTH = 768;
        public const int C_SCREEN_HEIGHT = 1024;

        public const float C_DELAY = 0.03f;

        public const string PREFAB_HEXAGON = "Hexagon";
        public const string PREFAB_POWER = "power_base";
        public const string TEXTASSET_LEVELS = "levels";

        public const string URL_OWL_LAB_GAMES = "https://play.google.com/store/apps/developer?id=Owl+lab";
        public const string URL_VOTE = "https://play.google.com/store/apps/developer?id=Owl+lab";

        public const string W_LEVELS = "level";
        public const string W_CURR_LEVEL = "curr_level";
        public const string W_TUT_1 = "tutorial1";
        public const string W_TUT_2 = "tutorial2";
        public const string W_TUT_3 = "tutorial3";
        public const string W_CONG = "cong";
        public const string W_VOTE = "vote";

        public const uint C_LEVEL_TO_SHOW_TUT = 0;
        public const uint C_LEVEL_TO_SHOW_VOTE = 4;
    }

}
