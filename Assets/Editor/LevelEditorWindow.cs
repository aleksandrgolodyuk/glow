using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace owllab {
    public class LevelEditorWindow : EditorWindow {

        private const float BUTTON_SIZE = 1f;
        private const float PICK_SIZE = 1f;

        private const string A_LEVELS = "Assets/Resources/levels.json";
        private const string A_HEX = "Assets/Resources/Hexagon.prefab";
        private const string A_BOARD_MANAGER = "Assets/Resources/board.prefab";
        private const string A_POWER = "Assets/Resources/power_base.prefab";

        private bool isUpPressed = false;
        private bool isDownPressed = false;
        private bool isLeftUpPressed = false;
        private bool isLeftDownPressed = false;
        private bool isRigtUpPressed = false;
        private bool isRigtDownPressed = false;

        private Cell activeHex = null;
        
        private Transform m_pBoard = null;
        private Transform m_pPower = null;

        private List<LevelData> m_pLevels;

        private string[] m_pLevelStr = new string[1]{""};
        private int m_pLevelIndex;

        private BoardController m_boardController;
        private Dictionary<PowerType, GUIContent> m_powerBtnTexture = new Dictionary<PowerType, GUIContent>();

        private Dictionary<PowerType, Sprite> m_powerSprites = new Dictionary<PowerType, Sprite>();

        [MenuItem("Owl lab/Level Editor")]
        private static void Init() {
            LevelEditorWindow window = (LevelEditorWindow)EditorWindow.GetWindow(typeof(LevelEditorWindow));
            //window.Load();
        }

        private void OnGUI() {
            
            EditorGUILayout.BeginHorizontal();

            m_pLevelIndex = EditorGUILayout.Popup(m_pLevelIndex, m_pLevelStr);
            //m_boardController.currTest = m_pLevelIndex;
            //Debug.Log(m_boardController.m_currLevelTest);
            
            if (GUILayout.Button("Load")) Build();
            if (GUILayout.Button("Save")) Save();
            if (GUILayout.Button("Add")) Add();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Add 0,0 hex")) AddZeroHex();
            if (GUILayout.Button("Clear all")) Clear();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.LabelField("Add powers:");
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_powerBtnTexture[PowerType.Point])) AddPower(PowerType.Point);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.Up])) AddPower(PowerType.Up);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.Down])) AddPower(PowerType.Down);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UL])) AddPower(PowerType.UL);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DL])) AddPower(PowerType.DL);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UR])) AddPower(PowerType.UR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DR])) AddPower(PowerType.DR);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DL_UR])) AddPower(PowerType.DL_UR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UL_DR])) AddPower(PowerType.UL_DR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.Up_Down])) AddPower(PowerType.Up_Down);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UL_UR])) AddPower(PowerType.UL_UR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.U_DR])) AddPower(PowerType.U_DR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UR_D])) AddPower(PowerType.UR_D);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DL_DR])) AddPower(PowerType.DL_DR);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UL_D])) AddPower(PowerType.UL_D);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DL_U])) AddPower(PowerType.DL_U);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.UL_D_UR])) AddPower(PowerType.UL_D_UR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.DL_U_DR])) AddPower(PowerType.DL_U_DR);
            if (GUILayout.Button(m_powerBtnTexture[PowerType.ALL])) AddPower(PowerType.ALL);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Test")) {
                Clear();
                //Player.currentLevel = m_pLevelIndex;
                //m_boardController.currTest = m_pLevelIndex;
                //Debug.Log("loading... " + Player.currentLevel);
                EditorApplication.isPlaying = true;
            }
            EditorGUILayout.EndHorizontal();
             
        }

        private void OnEnable() {
            Load();
            EditorApplication.playmodeStateChanged += ClearBeforePlay;
            
            SceneView.onSceneGUIDelegate += OnSceneGUI;
            
            //get board gameObject
            GameObject go = GameObject.Find("board");
            if (go == null) {
                go = (GameObject)AssetDatabase.LoadAssetAtPath(A_BOARD_MANAGER, typeof(GameObject));
                m_pBoard = (Transform)Instantiate(go, Vector3.zero, Quaternion.identity);
                m_pBoard.name = "board";

            } else {
                m_pBoard = go.transform;
            }

            m_boardController = m_pBoard.GetComponent<BoardController>();

            //get powers gameObject
            go = GameObject.Find("powers");
            if (go == null) {
                //TODO: create power empty GO
                go = new GameObject("powers");
            } else {
                m_pPower = go.transform;
            }

            m_powerBtnTexture[PowerType.Point] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_point.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.Up] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_002.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.Down] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_005.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UL] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_001.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DL] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_006.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_003.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_004.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DL_UR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_007.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UL_DR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_008.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.Up_Down] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_009.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UL_UR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_010.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.U_DR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_011.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UR_D] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_012.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DL_DR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_013.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UL_D] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_014.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DL_U] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_015.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.UL_D_UR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_016.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.DL_U_DR] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_017.png", typeof(Texture)));
            m_powerBtnTexture[PowerType.ALL] = new GUIContent((Texture)AssetDatabase.LoadAssetAtPath("Assets/Resources/lights_018.png", typeof(Texture)));
        }

        private void OnDisable() {
            SceneView.onSceneGUIDelegate -= OnSceneGUI;
            EditorApplication.playmodeStateChanged -= ClearBeforePlay;
        }

        private void OnSelectionChange() {
            GameObject select = Selection.activeGameObject;
            if (select != null) {
                Debug.Log("active GO: " + select.name);
                activeHex = select.GetComponent<Cell>() as Cell;
                Debug.Log("active.q,r: " + activeHex.q + ", " + activeHex.r);
                if (select.name == "Hexagon1") {

                }
            }
        }

        private void ClearBeforePlay() {
            //Debug.Log("callback");
            //if (EditorApplication.is) { 
            //    Clear(); 
            //    Debug.Log("is playing..."); 
            //}
            
        }

        private void OnSceneGUI(SceneView view) {
            if (Selection.activeGameObject != null && activeHex != null) {
                
                //GameObject target = Selection.activeGameObject;
                //activeHex = target.GetComponent<Cell>() as Cell;
                //Debug.Log("active,q,r: " + activeHex.q + " " + activeHex.r);

                isUpPressed = Handles.Button(activeHex.transform.position + new Vector3(0f, 2f, 0f),
                    Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

                isDownPressed = Handles.Button(activeHex.transform.position + new Vector3(0f, -2f, 0f),
                    Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

                isLeftUpPressed = Handles.Button(activeHex.transform.position + new Vector3(1.7f, 1.3f, 0f),
                    Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

                isLeftDownPressed = Handles.Button(activeHex.transform.position + new Vector3(1.7f, -1.3f, 0f),
                    Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

                isRigtUpPressed = Handles.Button(activeHex.transform.position + new Vector3(-1.7f, 1.3f, 0f),
                    Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

                isRigtDownPressed = Handles.Button(activeHex.transform.position + new Vector3(-1.7f, -1.3f, 0f),
                   Quaternion.identity, BUTTON_SIZE, PICK_SIZE, DrawFunc);

            }
        }

        private void DrawFunc(int controlId, Vector3 position, Quaternion rotation, float size) {
            Handles.CubeCap(controlId, position, rotation, size);
        }

        private void Update() {
            if (isUpPressed) AddHex(new Vector2(0f, -1f));
            if (isDownPressed) AddHex(new Vector2(0f, 1f));

            if (isLeftUpPressed) AddHex(new Vector2(1f, -1f));
            if (isLeftDownPressed) AddHex(new Vector2(1f, 0f));

            if (isRigtUpPressed) AddHex(new Vector2(-1f, 0f));
            if (isRigtDownPressed) AddHex(new Vector2(-1f, 1f));
        }

        private void AddHex(Vector2 offset) {
            if (activeHex == null) return;

            GameObject hex = (GameObject)AssetDatabase.LoadAssetAtPath(A_HEX, typeof(GameObject));

            int q = activeHex.q +  (int)offset.x;
            int r = activeHex.r + (int)offset.y;

            Vector2 point = GlowUtility.HexToPixel(new HexData(q, r));

            Vector3 pos = new Vector3(point.x, point.y, 0f);
            GameObject go = (GameObject) Instantiate(hex, pos, Quaternion.identity);
            Cell cell = go.GetComponent<Cell>();
            cell.data = new CellData(CellType.Basic, q, r);

            Selection.activeGameObject = go;
            go.transform.parent = m_pBoard;
            //Debug.Log("add hex:" + cell.q + ", " + cell.r);


        }

        private void AddZeroHex() {
            GameObject hex = (GameObject)AssetDatabase.LoadAssetAtPath(A_HEX, typeof(GameObject));
            
            GameObject go = (GameObject) Instantiate(hex, Vector3.zero, Quaternion.identity);
            Cell cell = go.GetComponent<Cell>();
            cell.data = new CellData(CellType.Basic, 0, 0);
            go.transform.parent = m_pBoard;
        }

        private void AddPower(PowerType type) {
            GameObject go = GetPowerByType(type);
            go = (GameObject)Instantiate(go, Vector3.zero, Quaternion.identity);
            Power power = go.GetComponent<Power>();
            power.Init();
            power.type = type;
            
            //set actul sprite for this types of sprite
            Texture2D texture = (Texture2D)m_powerBtnTexture[type].image;
            Sprite sp = Sprite.Create(texture, power.sprite.rect, new Vector2(0.5f,0.5f), 23f);
            power.sprite = sp;
            
            go.transform.parent = m_pPower;
        }

        private GameObject GetPowerByType(PowerType type) {
            GameObject go = (GameObject)AssetDatabase.LoadAssetAtPath(A_POWER, typeof(GameObject));
            //Power power = go.GetComponent<Power>();
            //power.type = type;
            //power.sprite = m_powerSprites[type];
            return go;
        }

        private void Clear() {

            while (m_pBoard.childCount > 0)
                DestroyImmediate(m_pBoard.GetChild(0).gameObject);
            

            while(m_pPower.childCount>0) {
                DestroyImmediate(m_pPower.GetChild(0).gameObject);
            }
        }

        private void Save() {

            //update current   
            Cell[] cells = m_pBoard.GetComponentsInChildren<Cell>();
            Power[] pow = m_pPower.GetComponentsInChildren<Power>();
            
            CellData[] c = new CellData[cells.Length];
            for (int i = 0; i < cells.Length; ++i) {
                c[i] = new CellData(cells[i].type, cells[i].q, cells[i].r);
            }

            PowerData[] p = new PowerData[pow.Length];
            for (int i = 0; i < pow.Length; ++i) {
                p[i] = new PowerData(pow[i].type);
                Debug.Log(p[i].type);
            }
            m_pLevels[m_pLevelIndex] = new LevelData(c, p);
            
            //save all
            Dictionary<string, object> dict = new Dictionary<string, object>();
            Dictionary<string, object> level;

            for (int i = 0; i < m_pLevels.Count; ++i) {
                level = new Dictionary<string, object>();

                //powers
                List<object> powers = new List<object>();
                for (int j = 0; j < m_pLevels[i].powers.Length; ++j) {
                    powers.Add((int)m_pLevels[i].powers[j].type);
                }
                level["powers"] = powers;

                //cells
                List<object> res = new List<object>();

                for (int j = 0; j < m_pLevels[i].cells.Length; ++j) {
                    List<object> hex = new List<object>();
                    hex.Add((int)m_pLevels[i].cells[j].type);
                    hex.Add(m_pLevels[i].cells[j].q);
                    hex.Add(m_pLevels[i].cells[j].r);
                    res.Add(hex);
                }
                level["cells"] = res;

                dict[i.ToString()] = level;
            }

            string asset = Json.Serialize(dict);
            File.WriteAllText(A_LEVELS, asset);
            AssetDatabase.Refresh();
        }

        private void Build() {
            Clear();
            GameObject prefab = (GameObject) AssetDatabase.LoadAssetAtPath(A_HEX, typeof(GameObject));
            

            //hex
            for (int i = 0; i < m_pLevels[m_pLevelIndex].cells.Length; ++i) {
                Vector3 pos = GlowUtility.HexToPixel(m_pLevels[m_pLevelIndex].cells[i].data);
                GameObject go = (GameObject)Instantiate(prefab, pos, Quaternion.identity);
                Cell cell = go.GetComponent<Cell>();
                cell.data = m_pLevels[m_pLevelIndex].cells[i];
                go.transform.parent = m_pBoard;
            }

            //powers
            GameObject powerPrefab = GetPowerByType(PowerType.Point);
            int count = m_pLevels[m_pLevelIndex].powers.Length;
            float xStart;
            float r = 1.5f + 1;
            if (count%2== 0) {
                xStart = -(r * 0.5f) * (count - 1);
                Debug.Log("true");
            } else {
                Debug.Log("false");
                xStart = -((int)(count * 0.5f)) * r;
            }

            for (int i = 0; i < count; ++i) {
                Vector3 pos = new Vector3(0,0,0);
                pos.x = xStart + i * r;
                GameObject go = (GameObject)Instantiate(powerPrefab, pos, Quaternion.identity);
                Power power = go.GetComponent<Power>();
                power.type = m_pLevels[m_pLevelIndex].powers[i].type;
                go.transform.parent = m_pPower;
            }
        }

        private void Add() {
            CellData[] cells = new CellData[1] { new CellData(CellType.Basic, 0, 0) };
            PowerData[] powers = new PowerData[1] { new PowerData(PowerType.Point) };
            LevelData level = new LevelData(cells, powers);
            m_pLevels.Add(level);
            m_pLevelIndex++;

            Save();
            Load();
        }

        private void Load() {
            m_pLevels = new List<LevelData>();

            TextAsset asset = (TextAsset)AssetDatabase.LoadAssetAtPath(A_LEVELS, typeof(TextAsset));
            string levelsStr = asset.ToString();

            Dictionary<string, object> dict = (levelsStr == "") ? null : Json.Deserialize(levelsStr) as Dictionary<string, object>;
            if (dict == null || dict.Count == 0) {
                Debug.Log("null");
                m_pLevelStr = new string[1]{""};
            } else {
                Debug.Log("Load levels");
                m_pLevels = new List<LevelData>(GlowUtility.LoadLevel(dict));
                m_pLevelStr = new string[m_pLevels.Count];
                for (int i = 0; i < m_pLevels.Count; ++i) {
                    m_pLevelStr[i] = i.ToString();
                }
            }

            m_powerSprites = new Dictionary<PowerType, Sprite>();
            m_powerSprites[PowerType.Point] = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Resources/power_0.png", typeof(Sprite));
            m_powerSprites[PowerType.Up] = (Sprite)AssetDatabase.LoadAssetAtPath("Assets/Resources/power_1.png", typeof(Sprite));
        }

    }
}